@extends('includes.master')
@section('main-content')
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    @if(Session::get('Success'))
            <div class="alert alert-success my-widget-alert" role="alert">
              <i class="fa fa-check"></i> {{ session()->get('Success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          @if(Session::get('Error'))
            <div class="alert alert-danger my-widget-alert" role="alert">
              <i class="fa fa-exclamation-triangle"></i> {{ session()->get('Error')}}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
    <div class="widget-container fluid-height clearfix my-container">
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>PRICE</th>
            <th>TYPE</th>
            <th>SIZE</th>
            <th>DESCRIPTION</th>
            <th>ACTION</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($pizza_list as $pizzas)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucwords($pizzas->price)}}
              </td>
              <td>
                {{ucfirst($pizzas->type)}}
              </td>
              <td>
                {{ucfirst($pizzas->size)}}
              </td>
              <td>
                {{ucfirst($pizzas->description)}}
              </td>
              <td>
                <a href="{{route('get.pizza.update', ['id' => $pizzas->id])}}" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="{{route('get.pizza.delete', ['id' => $pizzas->id])}}" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop