@extends('includes.master')
@section('main-content')
<?php
$size = [
  'small' => "Small",
  'medium' => "Medium",
  'large' => "Large"
];
?>
@if($data['get_pizza_by_id'])
<?php $edit_flag = true; ?>
@endif
<!-- <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#product">Product</a></li>
  <li><a data-toggle="tab" href="#safety">Safety</a></li>
  <li><a data-toggle="tab" href="#misc">Miscellaneous</a></li>
</ul> -->

<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="row widget-alert-wrapper product-alerts">
        <div class="widget-alert col-md-12 variant-widget-alert">
          @if(Session::get('Success'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! New Product has been successfully registered.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if($errors->has('input-type') || $errors->has('input-size') || $errors->has('input-price') || $errors->has('input-description'))
          <div class="alert alert-danger my-widget-alert" role="alert">
            <i class="fa fa-exclamation-triangle"></i> Error! Form Validation Error! Please try again.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="tab-content">
              <div id="product" class="tab-pane fade in active">
                <div class="heading no-padding-lr">
                  <i class="fa fa-car"></i>{{isset($edit_flag) ? "Update" : "Add"}} Pizza
                </div>
                <div class="heading no-padding-lr gray">
                  <i class="fa fa-th-list"></i>Select Specifications
                </div>
                <div class="heading-info">
                  <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="type" class="control-label">Type *</label>
                      <input class="form-control" id="type" name="input-type" type="text" value="{{isset($edit_flag) ? $data['get_pizza_by_id'][0]->type : old('input-type')}}" placeholder="Type of Pizza">
                      <span class="my-form-error">{{$errors->first('input-type')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="size" class="control-label">Size *</label>
                      <select class="form-control" name="input-size" id="size">
                        <option value="">Please Select a Size</option>
                        @foreach($size as $key=>$value)
                          <option value="{{$key}}"@if(isset($edit_flag) && $data['get_pizza_by_id'][0]->size == $key) selected="selected"@endif>{{$value}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-size')}}</span>
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="price">Price *</label>
                      <input class="form-control" id="price" name="input-price" type="text" value="{{isset($edit_flag) ? $data['get_pizza_by_id'][0]->price : old('input-price')}}" placeholder="Price">
                      <span class="my-form-error">{{$errors->first('input-price')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="description">Description *</label>
                      <textarea class="form-control" id="description" name="input-description" placeholder="Description">{{isset($edit_flag) ? $data['get_pizza_by_id'][0]->description : old('input-description')}}</textarea>
                      <span class="my-form-error">{{$errors->first('input-price')}}</span>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
  @stop
