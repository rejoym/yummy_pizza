@extends('includes.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="heading">
        <i class="fa fa-area-chart"></i>Welcome to Dashboard Page
      </div>
      <div class="my-widget-content ">
        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil impedit molestiae illo. Aspernatur quasi, sed commodi, a aliquam distinctio voluptate!</span>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix my-container">
      <div class="widget-content padded clearfix">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel quisquam laboriosam veritatis fugit asperiores ducimus possimus dolorum ea excepturi. Quibusdam nulla maxime natus excepturi dolore eligendi consectetur aut velit quasi aliquam, vel perferendis quia, incidunt temporibus quae quas. Incidunt ea doloribus, quia culpa hic repellendus a. Ut iste impedit veniam.
        </p>
      </div>
    </div>
  </div>
</div>
@stop