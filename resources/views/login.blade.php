<!DOCTYPE html>
<html>
  <head>
    <title>
      YUMMY PIZZA - Log In
    </title>
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/hightop-font.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/main.js') }}" type="text/javascript"></script>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
  </head>
  <body class="login2">
    <!-- Login Screen -->
    <div class="login-wrapper">
      <h1 class="login-page-title"><span class="red">YUMMY</span> &nbsp;PIZZA</h1>
      <span class="login-page-sub-title">&mdash; Admin Panel &mdash;</span>
      <!-- @if(Session::get('loggedOut')) -->
        <p class="logged-out-msg"><i class="fa fa-check"></i> You have been successfully logged out!</p>
      <!-- @endif -->
      <form action="" method="post" class="login-form">
        {!! csrf_field() !!}
        <!-- @if($errors->has('useroremail') || $errors->has('password')) -->
          <label class="login-error"><i class="fa fa-exclamation-triangle"></i> Email and password fields are required!</label>
        <!-- @endif -->
        <!-- @if(Session::get('loginError')) -->
          <label class="login-error"><i class="fa fa-exclamation-triangle"></i> The credentials you supplied were not correct!</label>
        <!-- @endif -->
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-envelope"></i>
            </span>
            <input class="form-control" placeholder="Enter Email" type="text" name="useroremail">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <span class="input-group-addon">
              <i class="fa fa-lock"></i>
            </span>
            <input class="form-control" placeholder="Password" type="password" name="password">
          </div>
        </div>
        <!-- <a class="pull-right" href="#">Forgot password?</a> -->
        <!-- <div class="text-left">
          <label class="checkbox"><input type="checkbox" name="remember-me"><span>Keep me logged in</span></label>
        </div> -->
        <input class="btn btn-lg btn-primary btn-block login-submit" type="submit" value="Log in">
        <!-- <div class="social-login clearfix">
          <a class="btn btn-primary pull-left facebook" href="index.html"><i class="fa fa-facebook"></i>Login with facebook</a><a class="btn btn-primary pull-right twitter" href="index.html"><i class="fa fa-twitter"></i>Login with twitter</a>
        </div> -->
      </form>
      <!-- <p>
        Don't have an account yet?
      </p>
      <a class="btn btn-default-outline btn-block" href="signup2.html">Sign up now</a>
    </div> -->
    <!-- End Login Screen -->
  </body>
</html>