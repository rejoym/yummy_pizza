<!DOCTYPE html>
<html>
<head>
  <title>
    Yummy Pizza - Dashboard
  </title>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/hightop-font.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/isotope.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/jquery.fancybox.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/fullcalendar.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/wizard.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/datatables.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/datepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/timepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/colorpicker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/bootstrap-switch.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/bootstrap-editable.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/daterange-picker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/typeahead.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/summernote.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/ladda-themeless.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/social-buttons.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/jquery.fileupload-ui.css') }}" media="screen" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/dropzone.css') }}" media="screen" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/nestable.css') }}" media="screen" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/pygments.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
   <link href="{{ asset('assets/backend/css/ionicons.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
  <link href="{{ asset('assets/backend/css/custom.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('assets/backend/css/color/green.css') }}" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
  <link href="{{ asset('assets/backend/css/color/orange.css') }}" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
  <link href="{{ asset('assets/backend/css/color/magenta.css') }}" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
  <!-- <meta name="_base_url" content="{{ url('/') }}"> -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
</head>
<body class="page-header-fixed bg-1">
  <div class="modal-shiftfix main-body-wrapper">
    <!-- Navigation -->
    <div class="navbar navbar-fixed-top scroll-hide">
      <div class="container-fluid top-bar">
        <div class="pull-right">
          <ul class="nav navbar-nav pull-right">
            <!-- <li class="dropdown notifications hidden-xs">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="hightop-flag"></span>
                <div class="sr-only">
                  Notifications
                </div>
                <p class="counter">
                  4
                </p>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">
                  <div class="notifications label label-success">
                    New
                  </div>
                  <p>
                    New user added: Jane Smith
                  </p></a>

                </li>
                <li><a href="#">
                  <div class="notifications label label-success">
                    New
                  </div>
                  <p>
                    Sales targets available
                  </p></a>

                </li>
                <li><a href="#">
                  <p>
                    New performance metric added
                  </p></a>

                </li>
                <li><a href="#">
                  <div class="notifications label label-success">
                    New
                  </div>
                  <p>
                    New growth data available
                  </p></a>

                </li>
              </ul>
            </li> -->
            <!-- <li class="dropdown messages hidden-xs">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="hightop-envelope"></span>
                <div class="sr-only">
                  Messages
                </div>
                <p class="counter">
                  3
                </p>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">
                  <img width="34" height="34" src="{{ asset('assets/backend/images/avatar-male2.png') }}" />Could we meet today? I wanted...</a>
                </li>
                <li><a href="#">
                  <img width="34" height="34" src="{{ asset('assets/backend/images/avatar-female.png') }}" />Important data needs your analysis...</a>
                </li>
                <li><a href="#">
                  <img width="34" height="34" src="{{ asset('assets/backend/images/avatar-male2.png') }}" />Buy Se7en today, it's a great theme...</a>
                </li>
              </ul>
            </li> -->
            <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <img width="34" height="34" src="{{asset('assets/backend/images/avatar-male.jpg')}}" />{{Auth::user()->username}}<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">
                  <i class="fa fa-user"></i>My Account</a>
                </li>
                <li><a href="{{ route('get.logout') }}">
                  <i class="fa fa-sign-out"></i>Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <button class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="logo logo-title" target="_blank" href="{{ url('/') }}"><span class="red">Yummy</span> Pizza <span class="sub-title">&nbsp;"admin"</span></a>
      </div>
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
            <ul class="nav main-b-menu">
              <li>
                <a class="{{ Helper::setActiveMenu('dashboard') }}" href="{{ url('admin/home') }}"><i class="ion-md-home admin-menu-icon"></i>Dashboard</a>
              </li>
              <li class="dropdown">
              <a data-toggle="dropdown" href="#" class="{{ Helper::setActiveMenu('pizzas') }}">
                <i class="ion-ios-apps admin-menu-icon"></i>Pizzas<b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="{{ route('get.pizza.list' )}}">List/ Edit Pizzas</a>
                </li>
                <li>
                  <a href="{{ route('get.pizza.create' )}}">Add New Pizza</a>
                </li>

              </ul>


            </li>
            <li class="dropdown">
              <a data-toggle="dropdown" href="#" class="{{ Helper::setActiveMenu('orders') }}">
                <i class="ion-ios-document admin-menu-icon"></i>Orders<b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="">Molding</a>
                </li>
                <li>
                  <a href="">Cladding</a>
                </li>
              </ul>
            </li>



      <!-- <div class="container-fluid main-nav clearfix">
        <div class="nav-collapse">
          <ul class="nav">
            <li>
              <a class="current" href="index.html"><span aria-hidden="true" class="hightop-home"></span>Dashboard</a>
            </li>
          </ul>
        </div>
      </div> -->
    </div>