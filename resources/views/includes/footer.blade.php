    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/custom.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/raphael.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/selectivizr-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.mousewheel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.vmap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.vmap.world.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/fullcalendar.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/gcal.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/datatable-editable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.easy-pie-chart.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/excanvas.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.isotope.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/isotope_extras.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/modernizr.custom.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/styleswitcher.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/wysiwyg.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/typeahead.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/summernote.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.inputmask.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.validate.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-fileupload.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-timepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/typeahead.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/spin.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/ladda.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/mockjax.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/bootstrap-editable.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/xeditable-demo-mock.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/xeditable-demo.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/address.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/daterange-picker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/date.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/skycons.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/fitvids.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/dropzone.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/jquery.nestable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/backend/js/respond.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        @yield('page-variant-js')
    </script>
    <script type="text/javascript">
        @yield('page-js')
    </script>
    <script type="text/javascript">
        @yield('page-product-js')
    </script>
    <script type="text/javascript">
        @yield('page-checkbox-change')
    </script>
    <script type="text/javascript">
        @yield('product-image')
    </script>
  </body>
</html>