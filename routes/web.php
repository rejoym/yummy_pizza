<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group( function() {
  Route::get('/login', ['uses' => 'Auth\LoginController@getAdminLogin', 'as' => 'login']);
  Route::post('/login', ['uses' => 'Auth\LoginController@postAdminLogin', 'as' => 'post.login']);
  Route::get('/logout', ['uses' => 'Auth\LoginController@getLogout', 'as' => 'get.logout']);
  Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', ['uses' => 'backend\HomeController@getIndex', 'as' => 'get.home']);
    Route::get('/list-pizza', ['uses' => 'backend\PizzaController@getList', 'as' => 'get.pizza.list']);
    Route::get('/create-pizza', ['uses' => 'backend\PizzaController@getCreatePizza', 'as' => 'get.pizza.create']);
    Route::post('/create-pizza', ['uses' => 'backend\PizzaController@postCreatePizza', 'as' => 'post.pizza.create']);
    Route::get('/delete-pizza/{id}', ['uses' => 'backend\PizzaController@deletePizza', 'as' => 'get.pizza.delete']);
    Route::get('/update-pizza/{id}', ['uses' => 'backend\PizzaController@getEditPizza', 'as' => 'get.pizza.update']);
    Route::post('/update-pizza/{id}', ['uses' => 'backend\PizzaController@postEditPizza', 'as' => 'post.pizza.update']);
  });
});
