<?php

namespace App\Http\Controllers\Api\v1;

use DB;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Pizza as Pizza;

class ApiController extends Controller
{
    /**
     * variable to store the format of json response
     * @var [array]
     */
    private $response = ['data' => '', 'status' => '', 'message' => ''];

    private function fetchResponse($data, $statusCode, $message)
    {
        $this->response['data'] = $data;
        $this->response['status'] = $statusCode;
        $this->response['message'] = $message;
        return Response::JSON($this->response);
    }

    /**
     * get pizza by id
     * @param  [integer] $id [description]
     * @return [object]     [description]
     */
    public function getPizzaById($id)
    {
      return Pizza::find($id);
    }

    /**
     * [getOldProducts description]
     * @return [type] [description]
     */
    public function getPizzas()
    {
        try
        {
            $pizza = DB::table('pizzas')
                             ->select('*')
                             ->simplePaginate(10);//limit the number to 10
            if(count($pizza) > 0)
                return $this->fetchResponse($pizza, 200, 'Old Product List');
            else
                return $this->fetchResponse($pizza, 200, 'No data exists');
        }
        catch(\Exception $e)
        {
            echo $e;
            return "Error Processing request";
        }
    }
}
