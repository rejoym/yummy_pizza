<?php

namespace App\Http\Controllers\Auth;

use DB;
use \Session;
use \Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PostLoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Where to redirect users after unsuccessful login or logout
     * @var string
     */
    protected $loginPath = '/login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('getLogout');
    }

     /**
     * load the login page on get request
     * @return [View] login view page
     */
     public function getAdminLogin()
     {
         return view('login');
     }

     /**
      * post login request
      * @return [type] [description]
      */
     public function postAdminLogin(PostLoginRequest $request)
     {
         $usernameoremail = $request->input('useroremail');
         $password = $request->input('password');
         $remember = $request->input('remember-me') ? true : false;
         //auth library to check whether the user input values of username and/or email and password match with that of the database
         //if true, store the user data in session
         if( (Auth::attempt(['username' => $usernameoremail, 'password' => $password], $remember)) || (Auth::attempt(['email' => $usernameoremail, 'password' => $password], $remember)) )
         {
             $this->loggedIn();
         }

         $request->session()->flash('loginError', 'Invalid credentials!!!');
         return redirect()->route('login')->withInput($request->except('password'));
     }

     /**
      * function to check user info
      * @return [view] [description]
      */
     public function loggedIn()
     {
         $user_info = $this->loggedInInfo();
         Session::put('user_info', $user_info);
         return redirect()->intended('admin/home');
     }

     /**
      * check if user exists
      * @return [view|array] [description]
      */
     public function loggedInInfo()
     {
         $id = Auth::id();
         $user_info_detail[] = DB::table('admins')
                                 ->select('*')
                                 ->where('id', $id)
                                 ->first();
         if(empty($user_info_detail))
         {
             Session::flash('loginErr', 'User Not Found!!');
             return redirect()->route('login');
         }
         return $user_info_detail;
     }


      public function getLogout(Request $request) {
         $user = Auth::user();
         if($user):
             Auth::logout();
             $request->session()->flash('loggedOut', 'You have sucessfully logged out.');
             $request->session()->forget('user_info');
         endif;

         return redirect()->route('login');
     }
}
