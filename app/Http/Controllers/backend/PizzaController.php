<?php

namespace App\Http\Controllers\backend;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostPizzaRequest;
use App\Models\Pizza as Pizza;

class PizzaController extends Controller
{
    private $pizza_model;
    /**
     * constructor to call the parent's constructor and also instantiate the Pizza class
     * @return none
     */
    public function __construct()
    {
      // parent::__construct();
      $this->pizza_model = new Pizza;
    }

    /**
     * function to list all the pizza
     * @return [view] [returns the view page of the list pizza page]
     */
    public function getList()
    {
      $pizza_list = $this->pizza_model->getAllPizza();
      return view('pizza.list_pizza', compact('pizza_list'));
    }

    /**
     * function to load the view page of create pizza page
     * @return [view] [description]
     */
    public function getCreatePizza()
    {
      return view('pizza.create_pizza');
    }

    /**
     * function to insert pizza data into the database
     * @param  PostPizzaRequest $request [validation request]
     * @return [route]                    [the route of the view page to be loaded after success or error]
     */
    public function postCreatePizza(PostPizzaRequest $request)
    {
      try
      {
        $price = $request->input('input-price');
        $type = $request->input('input-type');
        $size = $request->input('input-size');
        $description = $request->input('input-description');
        $to_insert = [
          'price' => $price,
          'type' => $type,
          'size' => $size,
          'description' => $description
        ];
        $pizza_insert = $this->pizza_model->createPizza($to_insert);
        $request->session()->flash('Success');
        return redirect()->route('post.pizza.create');
      }
      catch(\Exxeption $e)
      {
        return redirect()->back()->withError('Error');
      }

    }

    /**
     * function to delete selected pizza
     * @param  [integer] $id [description]
     * @return [route]     [route of the view page]
     */
    public function deletePizza($id)
    {
      try
      {
        $delete_flag = $this->pizza_model->deletePizza($id);
        if($delete_flag)
        {
          Session::flash('Success', 'Selected Pizza successfully deleted !');
          return redirect()->route('get.pizza.list');
        }
        else
        {
          Session::flash('Error', 'Error Deleting Record !!');
          return redirect()->route('get.pizza.list');
        }
      }
      catch(\Exception $e)
      {
        return redirect()->back()->withError('Error');
      }
    }

    /**
     * function to load view page of edit pizza
     * @param  [integer] $id [description]
     * @return [view]     [description]
     */
    public function getEditPizza($id)
    {
      $data['get_pizza_by_id'] = $this->pizza_model->getPizzaById($id);
      return view('pizza.create_pizza', compact('data'));
    }


    public function postEditPizza(PostPizzaRequest $request, $id)
    {
      $pizza_id = Pizza::findOrFail($id)['id'];
      $to_edit = [
        'price' => $request->input('input-price'),
        'type' => $request->input('input-type'),
        'size' => $request->input('input-size'),
        'description' => $request->input('input-description')
      ];
      $edit_flag = $this->pizza_model->updatePizza($pizza_id, $to_edit);
      if($edit_flag == TRUE)
      {
        $request->session()->flash('Success', 'Successfully updated the pizza');
        return redirect()->route('get.pizza.list');
      }
      else
      {
        return redirect()->back()->withError('Error');
      }
    }
}
