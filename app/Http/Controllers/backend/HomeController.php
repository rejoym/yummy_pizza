<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * landing page of home
     *
     * @return  [View]  [return landing page of dashboard]
     */
    public function getIndex()
    {
        return view('dashboard.home');
    }
}
