<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostPizzaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-price' => 'required',
            'input-type' => 'required',
            'input-size' => 'required',
            'input-description' => 'required'
        ];
    }

    /**
     * function to display custom validation error messages
     * @return [array] [array of custom messages]
     */
    public function messages()
    {
        return [
            'input-price.required' => 'Price is required',
            'input-type.required' => 'Type is required',
            'input-size.required' => 'Size is required',
            'input-description.required' => 'Description is required'
        ];
    }
}
