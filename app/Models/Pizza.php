<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    /**
     * attributes that are mass assignable
     * @var [array]
     */
    protected $fillable = ['price', 'type', 'size', 'description'];

    public function orderPizza()
    {
      return $this->hasMany('App\Models\Order_pizza');
    }

    public function getAllPizza()
    {
      return DB::table('pizzas')->get();
    }


    public function createPizza($data)
    {
        $insert_pizza = $this->create($data);
        if($insert_pizza)
            return TRUE;
        else
            return FALSE;
    }

    /**
     * function to delete a record of the pizza
     * @param  [integer] $id [the unique id of the record which is intended to be deleted]
     * @return [query result]     [description]
     */
    public function deletePizza($id)
    {
        return $this->where('id', $id)
                    ->delete();
    }

    /**
     * function to retrieve value based on id
     * @param  [integer] $id [description]
     * @return [array]     [result of the query]
     */
    public function getPizzaById($id)
    {
        return $this->where('id', $id)->get();
    }

    /**
     * function to update pizza records
     * @param  [integer] $id [description]
     * @return [array]     [result of the updated query]
     */
    public function updatePizza($id, $data)
    {
        $edit_pizza = $this->where('id', $id)
                           ->update($data);
        if($edit_pizza)
            return true;
        else
            return false;
    }
}
