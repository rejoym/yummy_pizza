<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order_pizza extends Model
{
    /**
     * attributes that are mass assignable
     * @var [array]
     */
    protected $fillable = ['order_id', 'pizza_id'];

    /**
     * function to establish relation between order and order_pizza
     * @return [array] [description]
     */
    public function order()
    {
      return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    /**
     * function to establish relation between order and pizza table
     * @return [array] [description]
     */
    public function pizza()
    {
      return $this->belongsTo('App\Models\Pizza', 'pizza_id', 'id');
    }
}
