<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * attributes that are mass assignable
     * @var [array]
     */
    protected $fillable = ['customer_id', 'order_date', 'total_price', 'payment_method', 'delivery_status', 'shipping_address', 'delivery_date'];

    /**
     * establishing relation between customer table and order table
     * @return [type] [description]
     */
    public function customer()
    {
      return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    /**
     * relation between order and order_pizza table
     * @return [type] [description]
     */
    public function order_pizzas()
    {
      return $this->hasMany('App\Models\Order_pizza');
    }
}
