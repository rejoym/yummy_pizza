<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * attributes that are mass assignable
     * @var [array]
     */
    protected $fillable = ['name', 'email', 'mobile_no', 'address', 'username', 'password'];

    /**
     * function to establish relation between order and customer table
     * @return [array] [description]
     */
    public function orders()
    {
      return $this->hasMany('App\Models\Order');
    }
}
